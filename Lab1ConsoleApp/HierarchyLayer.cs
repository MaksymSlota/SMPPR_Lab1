﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1ConsoleApp
{
    public class HierarchyLayer
    {
        public List<MPP> Matrices { get; private set; }
        private List<double> _previousLayerWeights;

        private List<double> _globalWeights;

        public HierarchyLayer(string input, List<double> previousLayerWeights)
        {
            Matrices = new List<MPP>();
            _globalWeights = new List<double>();
            _previousLayerWeights = previousLayerWeights;

            input = input.Trim(Environment.NewLine.ToCharArray());
            foreach (var matrix in input.Split('*'))
            {
                Matrices.Add(new MPP(matrix));
            }
            CalculateGlobalWeight(_previousLayerWeights);
        }

        public List<double> GlobalWeights
        {
            get
            {
                if (_globalWeights.Count <= 0)
                {
                    CalculateGlobalWeight(_previousLayerWeights);
                }
                return _globalWeights;
            }
        }

        //distributive method
        private void CalculateGlobalWeight(List<double> wArray)
        {
            if (wArray.Count != Matrices.Count)
            {
                throw new ArgumentException("wArray is not valid");
            }

            for (int i = 0; i < Matrices[0].Count; i++)
            {
                double s = 0;
                for (int j = 0; j < wArray.Count; j++)
                {
                    double w_j = wArray[j];
                    var r_ij = Matrices[j].Weights[i];
                    s += w_j * r_ij;
                }
                _globalWeights.Add(s);
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            foreach (var matrix in Matrices)
            {
                result.Append(matrix.ToString());
            }
            return result.ToString();
        }

        public string GetAllInfo()
        {
            StringBuilder result = new StringBuilder();
            foreach (var matrix in Matrices)
            {
                result.Append(matrix.GetAllInfo());
            }
            result.Append("Global weights: [");
            foreach (var weight in GlobalWeights)
            {
                result.Append($" {weight.ToString("N3")};");
            }
            result.Append("]");

            return result.ToString();
        }
    }
}
