﻿using System.Collections.Generic;
using System.Linq;

namespace Lab1ConsoleApp
{
    public static class ListExtension
    {
        public static List<double> Normalize(this List<double> list)
        {
            //Normalize
            var summ = list.Sum();
            List<double> norm = new List<double>();
            for (int i = 0; i < list.Count; i++)
            {
                norm.Add(list[i] / summ);
            }
            return norm;
        }
    }
}
