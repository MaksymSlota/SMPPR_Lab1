﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1ConsoleApp
{
    public class HierachySystem
    {
        public int LayersCount { get; private set; }
        public List<HierarchyLayer> Layers { get; private set; }
        public HierachySystem()
        {
            Layers = new List<HierarchyLayer>();
        }

        public void LoadFromresourceFile()
        {
            Layers.Clear();
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Resources\Hierarchy.txt");
            String input = File.ReadAllText(path);
            var sections = input.Split('&');
            var inputFields = sections[0].Split(';').Select(Int32.Parse).ToList();
            LayersCount = inputFields.Count;

            //initialize layers
            for (int i = 1; i < sections.Length; i++)
            {
                if (i == 1)
                {
                    Layers.Add(new HierarchyLayer(sections[i], new List<double> { 1.0 } ));
                }
                else
                {
                    Layers.Add(new HierarchyLayer(sections[i], Layers[i - 2].GlobalWeights));
                }
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < Layers.Count; i++)           
            {
                result.Append($"*** {i + 1} layer ***\n");
                result.Append(Layers[i].ToString());
                result.Append("\n");
            }

            return result.ToString();
        }

        public string GetAllInfo()
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < Layers.Count; i++)
            {
                result.Append($"*** {i + 1} layer ***\n");
                result.Append(Layers[i].GetAllInfo());
                result.Append("\n");
            }

            return result.ToString();
        }
    }
}
