﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Lab1ConsoleApp
{
    public class MPP
    {
        List<List<double>> _matrix;
        List<double> _weights = new List<double>();
        private double _hcr;

        public MPP()
        {
            _matrix = new List<List<double>>();
        }

        public MPP(string inputMatrix)
        {
            _matrix = new List<List<double>>();
            ReadFromString(inputMatrix);

            CalculateWeights();
            CalculateHCR();
        }

        public double this[int i, int j]
        {
            get { return _matrix[i][j]; }
            set { _matrix[i][j] = value; }
        }
        public int Count { get { return _matrix.Count; } }

        public List<double> Weights
        {
            get
            {
                if(_weights.Count <= 0)
                {
                    CalculateWeights();
                }
                return _weights.Normalize();
            }
        }

        public double HCR
        {
            get
            {
                if (_hcr == 0)
                {
                    CalculateHCR();
                }
                return _hcr;
            }
        }

        private void CalculateHCR()
        {
            var n = _weights.Count;
            var s = (1 / _weights.Sum()) * n;
            var hci = ((s - n) * (n + 1)) / (n * (n - 1));
            var mhci = MRCI.GetMrciValue(n);
            _hcr = hci / mhci;
        }

        //CalculateWeidghts method
        public void CalculateWeights()
        {
            //sum of columns
            for (int i = 0; i < _matrix.Count; i++)
            {
                double s = 0;
                for (int j = 0; j < _matrix.Count; j++)
                {
                    s += _matrix[j][i];
                }
                _weights.Add(s);
            }

            // 1/s
            for (int i = 0; i < _weights.Count; i++)
            {
                _weights[i] = 1 / _weights[i];
            }
        }

        public void ReadFromresourceFile()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Resources\MPP.txt");
            String str = File.ReadAllText(path);
            ReadFromString(str);
        }

        public void ReadFromString(string input)
        {
            _matrix.Clear();
            Weights.Clear();
            int i = 0;
            foreach (var row in input.Split(new string[] { "\r\n" }, StringSplitOptions.None))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    _matrix.Add(new List<double>());
                    foreach (var col in row.Trim().Split(';'))
                    {
                        if (!string.IsNullOrEmpty(col))
                        {
                            _matrix[i].Add(double.Parse(col.Trim()));
                        }
                    }
                    i++;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            foreach (var row in _matrix)
            {
                foreach (var item in row)
                {
                    result.Append(item.ToString("N2") + "  ");
                }
                result.Append("\n");
            }
            return result.ToString();
        }

        public string GetAllInfo()
        {
            StringBuilder result = new StringBuilder();
            result.Append(ToString());

            result.Append("Weights = [");
            foreach (var weight in Weights)
            {
                result.Append($" {weight.ToString("N3")};");
            }
            result.Append("]\n");
            result.Append($"HCR = {HCR.ToString("N3")}\n");

            return result.ToString();
        }
    }
}
