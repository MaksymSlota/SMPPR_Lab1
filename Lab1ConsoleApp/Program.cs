﻿using System;
using System.Linq;

namespace Lab1ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            FirstTask();
            SecondTask();
            Console.ReadLine();
        }     
        
        public static void FirstTask()
        {
            Console.WriteLine("*****************\nFirst Part\n*****************");
            var _mpp = new MPP();
            _mpp.ReadFromresourceFile();
            Console.WriteLine(_mpp.ToString());
            var weights = _mpp.Weights.Normalize();
            foreach (var item in weights)
            {
                Console.Write(item.ToString("N3") + "  ");
            }
            Console.WriteLine();
            Console.WriteLine("Summ = " + weights.Sum().ToString());
            Console.WriteLine("HCR = " + _mpp.HCR);
        }

        public static void SecondTask()
        {
            Console.WriteLine("*****************\nSecond Part\n*****************");

            var hierarchySystem = new HierachySystem();
            hierarchySystem.LoadFromresourceFile();
            Console.WriteLine(hierarchySystem.GetAllInfo());
            Console.WriteLine();
        }
    }
}
