﻿using System.Collections.Generic;
using System.Linq;

namespace Lab1ConsoleApp
{
    public static class MRCI
    {
        public static Dictionary<int, double> table = new Dictionary<int, double>()
        {
            {1,0},
            {2,0.34},
            {3,0.52},
            {4,0.89},
            {5,1.11},
            {6,1.25},
            {7,1.35},
            {8,1.4},
            {9,1.45},
            {10,1.49}
        };

        public static double GetMrciValue(int n)
        {
            return n < 10 ? table[n] : table.Values.Last();
        }
    }
}
